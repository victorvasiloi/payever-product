<?php

namespace AppBundle\RabbitMQ\Consumer;


use AppBundle\Entity\Product;
use Doctrine\Common\Persistence\ObjectManager;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class StoreDeleted implements ConsumerInterface
{
    /**
     * @var ObjectManager
     */
    private $manager;

    /**
     * StoreDeleted constructor.
     * @param ObjectManager $manager
     */
    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $data = json_decode($msg->getBody(), true);

        if (!isset($data['storeId'])) {
            return ConsumerInterface::MSG_REJECT;
        }

        echo 'Message received', PHP_EOL, '- Store ID: ', $data['storeId'], PHP_EOL;

        $products = $this->manager->getRepository(Product::class)->findBy(['storeId' => $data['storeId']]);

        foreach ($products as $product) {
            echo '-- Product ID: ', $product->getId(), PHP_EOL;
            $this->manager->remove($product);
        }

        $this->manager->flush();

        echo 'Products removed.', PHP_EOL, PHP_EOL;

        return ConsumerInterface::MSG_ACK;
    }
}