<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Product;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadProducts implements FixtureInterface
{

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager): void
    {
        for ($i = 1; $i < 10; $i++) {
            $product = (new Product())
                ->setStoreId(random_int(1, 10))
                ->setCode('P' . str_pad($i, 3, '0'))
                ->setPrice(random_int(1000, 10000));

            $manager->persist($product);
        }

        $manager->flush();
    }
}