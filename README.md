Payever Product Microservice
===

This application is build using the **[API Platform Framework][1]**.

Prerequisites
---
- **[GIT][3]**
- **[Docker][2]**


Setup (Docker)
---
```
git clone https://bitbucket.org/victorvasiloi/payever-product.git
cd payever-product
docker-compose up -d
docker-compose exec web bin/console doctrine:schema:create
```
Load fixtures (optional): `docker-compose exec web bin/console doctrine:fixtures:load`

Access
---
Usually on [http://localhost:3001/app_dev.php/](), but the hostname may vary depending on your docker configuration

RabbitMQ
---
In order to test the communication within Product MS and Store MS using RabbitMQ,
connect Product MS's `web` service to Store MS's `rabbitmq` service,
or the Store MS's `web` service to Product MS's `rabbitmq` service.
That could be done in different ways, one of which is adding them to a common network:
```
docker network create payever
docker network add payever web-product
docker network add payever web-store
docker network add payever rabbitmq-store
```
then use `rabbitmq_host: rabbitmq-store` in Product MS's `app/config/parameters.yml`

Now enter the Product MS's `web` service and start the consumer: `bin/console rabbitmq:consumer store_deleted`
then delete a Store via the API, watch the logs in the console and check if the related products were deleted.

[1]: https://api-platform.com
[2]: https://docs.docker.com/engine/installation/
[3]: https://www.atlassian.com/git/tutorials/install-git